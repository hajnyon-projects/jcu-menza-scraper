# JCU Menza scraper

## ⚠️ Deprecated in favor of [slichta.vercel.app](https://slichta.vercel.app)

Simple script periodically scraping [https://menza.jcu.cz/](menza) site and displaying menus for humans. Using gitlab's scheduled ci scipts, pages and few npm packages (see https://hajnyon.gitlab.io/jcu-menza-scraper/#about).

## Usage

```bash
npm start # scrape menza's site
npm run build # scrapes and builds website with menu
```

## Installation

```bash
npm install
```
