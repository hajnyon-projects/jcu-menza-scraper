const puppeteer = require('puppeteer');
const fs = require('fs-extra');

const EXT = 'html';

module.exports = async function getHTML(OUT_FOLDER, FILE_NAMES) {
    await fs.mkdirp('tmp');

    const url = 'https://menza.jcu.cz/WebKredit/ZalozkaObjednavani.aspx';
    const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
    const page = await browser.newPage();
    await page.goto(url);

    await page.click('#Calendar1__CheckBoxPouzeJedenDen');

    page.click('#_ButtonSestavJidelnicek');

    try {
        await page.waitForNavigation();
    } catch (error) {
        console.error('Error: wait for navigation', error);
    }

    let data = await page.content();
    await fs.outputFile(`${OUT_FOLDER}/${FILE_NAMES.default}.${EXT}`, data);

    try {
        await page.select('#VydejnyComboBox1__DropDownListVydejny', '2');
        page.click('#_ButtonSestavJidelnicek');
    } catch (error) {
        console.error('Error: select and button', error);
    }

    try {
        await page.waitForNavigation();
    } catch (error) {
        console.error('Error: wait for navigation', error);
    }

    data = await page.content();
    await fs.outputFile(`${OUT_FOLDER}/${FILE_NAMES.fastFood}.${EXT}`, data);

    browser.close();
};
