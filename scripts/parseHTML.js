const fs = require('fs-extra');
const cheerio = require('cheerio');

const EXT_IN = 'html';
const EXT_OUT = 'json';

const KEY_LUNCH = 'Oběd - ';
const KEY_FAST = 'Minutka - ';
const KEY_SOUP = 'Polévka - ';

module.exports = async function parseHTML(OUT_FOLDER, FILE_NAMES) {
    const defaultFileIn = await fs.readFile(`${OUT_FOLDER}/${FILE_NAMES.default}.${EXT_IN}`);
    let $ = cheerio.load(defaultFileIn);
    const menus = {};
    let date = '';
    let type = '';
    // Default meals
    $('#Jidelnicek1__TableJidelnicek > tbody tr').each(function () {
        const _this = $(this);
        if (_this.attr('class') === undefined) {
            dateText = _this.children('td').text();
            if (dateText.indexOf(KEY_LUNCH) > -1) {
                date = dateText.replace(KEY_LUNCH, '');
                type = 'default';
                menus[date] = {
                    default: [],
                    soup: [],
                    fastFood: []
                };
            } else if (dateText.indexOf(KEY_SOUP) > -1) {
                type = 'soup';
            } else {
                type = '';
            }
        }
        if (['default', 'soup'].includes(type)) {
            if (_this.hasClass('TrPolozkaSeda') || _this.hasClass('TrPolozka')) {
                const name = _this.children('td.TdAltNazev').text();
                const prices = _this.children('td.TdCena, td.TdCenaSeda');
                const studentPrice = prices.first().text();
                const defaultPrice = prices.next().text();
                const allergens = _this.next().text();
                // remove duplicates
                if (menus[date][type].find((value) => value.name === name)) {
                    return;
                }
                menus[date][type].push({
                    name: name,
                    allergens: allergens,
                    price: {
                        student: studentPrice,
                        default: defaultPrice
                    }
                });
            }
        }
    });
    // FastFood
    const fastFoodFileIn = await fs.readFile(`${OUT_FOLDER}/${FILE_NAMES.fastFood}.${EXT_IN}`);
    $ = cheerio.load(fastFoodFileIn);
    $('#Jidelnicek1__TableJidelnicek > tbody tr').each(function () {
        const _this = $(this);
        if (_this.attr('class') === undefined) {
            dateText = _this.children('td').text();
            if (dateText.indexOf(KEY_FAST) > -1 || dateText.indexOf(KEY_LUNCH) > -1) {
                date = dateText.replace(KEY_FAST, '').replace(KEY_LUNCH, '');
                type = 'fastFood';
                menus[date] = menus[date] || {
                    default: [],
                    soup: [],
                    fastFood: []
                };
            } else {
                type = '';
            }
        }
        if (['fastFood'].includes(type)) {
            if (_this.hasClass('TrPolozkaSeda') || _this.hasClass('TrPolozka')) {
                const name = _this.children('td.TdAltNazev').text();
                const prices = _this.children('td.TdCena, td.TdCenaSeda');
                const studentPrice = prices.first().text();
                const defaultPrice = prices.next().text();
                const allergens = _this.next().text();
                // remove duplicates
                if (menus[date][type].find((value) => value.name === name)) {
                    return;
                }
                menus[date][type].push({
                    name: name,
                    allergens: allergens,
                    price: {
                        student: studentPrice,
                        default: defaultPrice
                    }
                });
            }
        }
    });
    const menusArr = [];
    for (const date in menus) {
        if (menus.hasOwnProperty(date)) {
            menusArr.push({ date: date, data: menus[date] });
        }
    }
    await fs.writeJSON(`${OUT_FOLDER}/${FILE_NAMES.default}.${EXT_OUT}`, menusArr);
};
