const getHTML = require('./getHTML');
const parseHTML = require('./parseHTML');

const OUT_FOLDER = 'tmp';
const FILE_NAMES = {
    default: 'default',
    fastFood: 'fastFood'
};

async function main() {
    await getHTML(OUT_FOLDER, FILE_NAMES);
    await parseHTML(OUT_FOLDER, FILE_NAMES);
}
main();
